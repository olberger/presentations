This presentation is based on my translation of Marek Goldmann's
Docker introduction slides available at
http://goldmann.pl/presentations/2013-red-hat-docker-introduction/index.fr.html
(sources
https://github.com/goldmann/goldmann.pl/tree/master/.presentations/2013-red-hat-docker-introduction),
with additions about Vagrant.

This content is available under the terms of the Creative Commons
Attribution-ShareAlike 3.0 Unported license (CC BY-SA 3.0)
http://creativecommons.org/licenses/by-sa/3.0/deed.en_US

The source was edited with org-mode and converted to reveal.js HTML
slides using org-reveal (https://github.com/yjwen/org-reveal/)
